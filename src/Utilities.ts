export function delay(ms: number): Promise<{}>;
export function delay(lower: number, upper: number): Promise<{}>;
export function delay(lower: number, upper?: number) {
    let ms = lower;
    if (upper !== undefined) {
        ms = Math.random() * (upper - lower) + lower;
    }

    return new Promise<{}>(resolve => setTimeout(resolve, ms));
}

export function pythagoras(a: number, b: number) {
    return Math.sqrt(a * a + b * b);
}