import { IWifiController, WifiController } from "./WifiController";
import { Node as VisNode, Properties } from "vis";
import { NodeKeeper } from "./NodeKeeper";
import { delay } from "./Utilities";
import { ISocket } from "./Socket";
import { IMessage, INodeInfo } from "./DTO";
import { INodeAlgorithm, INodeActions, Direction } from "./INodeAlgorithm";
import { AlgorithmA } from "./algorithms/AlgorithmA";

interface ExtendedVisNode extends VisNode {
    shapeProperties?: {
        borderDashes?: boolean | number[], // only for borders
        borderRadius?: number,     // only for box shape
        interpolation?: boolean,  // only for image and circularImage shapes
        useImageSize?: boolean,  // only for image and circularImage shapes
        useBorderWithImage?: boolean  // only for image shape
    };
}

export class Node {
    public readonly id: number;
    public readonly x: number;
    public readonly y: number;
    private readonly wifi: IWifiController;
    private readonly onChange: () => void;
    private isMaster: boolean = true;

    private algorithm: INodeAlgorithm;

    /**
     * Array containing the sockets for wifi connections.
     * [0..3] are connected to my AP.
     * [4] is my connection to another AP.
     */
    public readonly sockets: (ISocket | undefined)[]
        = new Array(5).fill(undefined);

    public constructor(id: number, x: number, y: number, onChange: () => void) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.onChange = onChange;
        this.wifi = new WifiController(this);

        let proxy: INodeActions = {
            sendMessage: (direction: Direction, message: IMessage) => {
                let socket = this.sockets[<number>direction];
                if (socket) {
                    socket.sendMessage(message);
                }
            },
            scanWiFi: this.wifi.scan,
            connectTo: this.connect,
            disconnect: this.disconnect,
            setIsMaster: (master: boolean) => {
                if (this.isMaster === master) {
                    return;
                }

                this.isMaster = master;
                this.onChange();
            }
        };

        this.algorithm = new AlgorithmA(this.name, proxy);
    }

    public toVis(): ExtendedVisNode {
        return {
            id: this.id,
            label: this.name,
            x: this.x * 50,
            y: this.y * 50,
            shapeProperties: {
                borderDashes: this.isMaster ? [] : [5, 5]
            }
        };
    }

    public get name() {
        return ("00" + this.id.toString(16)).substr(-2).toUpperCase();
    }

    private async connect(name: string): Promise<boolean> {
        // Connect and open a socket.
        let socket = await this.wifi.connect(name);
        if (!socket) {
            // If there is no socket, the connection failed.
            return false;
        }

        // Notify the change.
        this.onChange();
        
        // Set the socket.
        this.sockets[4] = socket;
        socket.receiveMessage = x => this.algorithm.messageReceived(Direction.Parent, x);
        return true;
    }

    private async disconnect(): Promise<void> {
        // Immediately stop listening to the socket.
        let socket = this.sockets[4];
        if(socket) socket.receiveMessage = undefined;
        this.algorithm.connectionEnded(Direction.Parent);

        // Call wifi disconnect.
        await this.wifi.disconnect();

        // Reset the socket.
        this.sockets[4] = undefined;
    }

    public newClient(socket: ISocket) {
        let id = this.sockets.indexOf(undefined);
        if (id < 0 || id > 3)
            throw 'No open connection spots.';
        
        this.sockets[id] = socket;
        socket.receiveMessage = x => this.algorithm.messageReceived(<Direction>id, x);
        this.algorithm.connectionStarted(<Direction>id);
    }

    public removedClient(socket: ISocket) {
        socket.receiveMessage = undefined;
        let id = this.sockets.indexOf(socket);
        this.sockets[id] = undefined;
        this.algorithm.connectionEnded(<Direction>id);
    }
}