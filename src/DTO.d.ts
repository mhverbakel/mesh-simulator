export type IMessage = IUpdateNodesMessage | IUpdateMasterMessage | IWelcomeMessage;

/**
 * The update master message is to inform children about a new master. This message
 * shall always travel downstream, and every node must pass this on to their
 * children (after incrementing the distance).
 */
export interface IUpdateMasterMessage {
    type: 'updateMaster';

    /**
     * The name of the new master.
     */
    master: string;

    /**
     * The new master-distance of the sender.
     */
    distance: number;

    /**
     * The list of added nodes.
     */
    newNodes?: INodeInfo[];

    /**
     * The list of removed node names.
     */
    lostNodes?: string[];
}

/**
 * The message sent by the parent in a new connection.
 */
export interface IWelcomeMessage {
    type: 'welcome',

    /**
     * The name of the master.
     */
    master: string;

    /**
     * The master-distance of the sender.
     */
    distance: number;

    /**
     * A list of nodes known to the sender, except in the direction of the receiver.
     * This list must include the sender at position 0.
     */
    nodes: INodeInfo[];
}

export interface IUpdateNodesMessage {
    type: "updateNodes",
    
    /**
    * The list of added nodes, if any.
    */
   newNodes?: INodeInfo[];

   /**
    * The list of removed node names, if any.
    */
   lostNodes?: string[];
}

export interface INodeInfo {
    /**
     * The name of the node.
     */
    name: string;

    /**
     * The distance between the node and it's master.
     */
    distance: number;
}