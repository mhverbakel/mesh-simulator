import { IMessage } from './DTO';

export interface ISocket {
    sendMessage(message: IMessage): void;
    receiveMessage?: (message: IMessage) => void;
}

class Socket implements ISocket {
    public receiveMessage?: (message: IMessage) => void;
    private other: Socket;

    public constructor(other?: Socket) {
        if (other) {
            this.other = other;
            other.other = this;
        }
    }

    public get counterpart() {
        return this.other;
    }

    public sendMessage(message: IMessage): void {
        // Send the message in a fire and forget fashion.
        setTimeout(() => {
            if (this.other.receiveMessage) {
                // Convert from and to JSON to ensure a deep copy.
                this.other.receiveMessage(JSON.parse(JSON.stringify(message)));
            }
        }, Math.random() * 500 + 100);
    }
}

export function createSocketPair(): { a: ISocket, b: ISocket } {
    let a = new Socket();
    let b = new Socket(a);

    return { a, b };
}