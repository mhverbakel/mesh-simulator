import { createSocketPair, ISocket } from "./Socket";
import { delay } from "./Utilities";
import { NodeKeeper } from "./NodeKeeper";
import { Node } from "./Node";

export interface IWifiResult {
    /**
     * The name of the network in range.
     */
    name: string;

    /**
     * Number between 0 and 1, indicating the percentage of signal strength.
     */
    strength: number;
}

export interface IWifiController {
    /**
     * Scan the networks that are near the current node.
     */
    scan(): Promise<IWifiResult[]>;

    /**
     * Connect to a specific node.
     * 
     * @param name The name of the network being connected to.
     * @returns A socket to the other node, or undefined if the connection failed.
     */
    connect(name: string): Promise<ISocket | undefined>;

    /**
     * Disconnects from the current node.
     */
    disconnect(): Promise<void>;
}

export class WifiController implements IWifiController {
    private readonly node: Node;

    public constructor(node: Node) {
        this.node = node;
    }

    public async scan(): Promise<IWifiResult[]> {
        await delay(1000, 2000);
        return NodeKeeper.getWifiResults(this.node)
            .map(x => ({
                name: x.node.name,
                strength: x.strength
            }));
    }

    public async connect(name: string): Promise<ISocket | undefined> {
        await delay(1000, 2000);
        let nodes = NodeKeeper
            .getWifiResults(this.node)
            .map(x => x.node)
            .filter(x => x.name === name);

        if (nodes.length === 0) {
            console.warn(`Node ${this.node.name} was unable to connect to ${name} because it is not in range.`);
            return undefined;
        }

        let from = this.node;
        let to = nodes[0];

        if (!NodeKeeper.connect(from, to)) {
            // In case of an error, simply return.
            return undefined;
        }

        let { a, b } = createSocketPair();
        to.newClient(a);
        return b;
    }

    public async disconnect(): Promise<void> {
        await delay(1000, 2000);
        let old = NodeKeeper.disconnect(this.node);
        if (!old) {
            return;
        }

        let mySocket = this.node.sockets[4];
        let theirSocket = <ISocket>(<any>mySocket).counterpart;
        old.removedClient(theirSocket);
    }
}