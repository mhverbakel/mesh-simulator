import { IMessage } from "./DTO";
import { IWifiResult } from "./WifiController";

export enum Direction {
    ChildA,
    ChildB,
    ChildC,
    ChildD,
    Parent
}

export let directions = Object.keys(Direction)
    .map(x => (Direction as any)[x])
    .filter(x => typeof x === "number") as Direction[];

export interface INodeAlgorithm {
    /**
     * Method that gets called when a new message is received.
     */
    messageReceived(direction: Direction, message: IMessage): void;

    /**
     * Method that gets called when a new connection is made.
     */
    connectionStarted(direction: Direction): void;

    /**
     * Method that gets called when a connection is broken.
     */
    connectionEnded(direction: Direction): void;
}

export interface INodeActions {
    /**
     * Send a message to the specified direction.
     */
    sendMessage(direction: Direction, message: IMessage): void;

    /**
     * Get information about the wifi networks around the node.
     */
    scanWiFi(): Promise<IWifiResult[]>;

    /**
     * Connect to the specified network.
     */
    connectTo(name: string): void;

    /**
     * Disconnect from the network.
     */
    disconnect(): void;

    /**
     * Set a value indicating whether this node is functioning as a master.
     */
    setIsMaster(master: boolean): void;
}