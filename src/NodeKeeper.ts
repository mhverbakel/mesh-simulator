import { Node } from "./Node";
import { delay, pythagoras } from "./Utilities";
import { DataSet, Edge, Node as VisNode } from "vis";

class NodeKeeperImplementation {
    public readonly count = 16;
    public readonly distance = 10;
    public readonly nodes: Node[] = [];
    public readonly visNodes: DataSet<VisNode> = new DataSet();
    private readonly links: (Node | undefined)[] = [];
    public readonly edges: DataSet<Edge> = new DataSet();

    public static readonly instance = new NodeKeeperImplementation();

    private constructor() {
        this.initialize();
    }

    private initialize() {
        for (let i = 0; i < this.count; i++) {
            this.nodes[i] = new Node(
                i,
                Math.random() * this.distance,
                Math.random() * this.distance,
                () => this.visNodes.update(this.nodes[i].toVis()));
            this.visNodes.add(this.nodes[i].toVis());
        }
    }

    public getWifiResults(me: Node) {
        return (<{node: Node, strength: number}[]>this.nodes.map(
            other => {
                if (other === me) {
                    return;
                }

                let strength = this.getSignalStrength(me, other);
                if (strength < 0.2) {
                    return;
                }

                return {
                    node: other,
                    strength: strength
                };
            })
            .filter(x => x !== undefined))
            .sort((a, b) => b.strength - a.strength);
    }

    public connect(from: Node, to: Node) {
        let previous = this.links[from.id];
        if (previous) {
            throw `Node ${from.name} tried to connect to ${to.name} while it was connected to ${previous.name}.`;
        }

        if (this.links.filter(x => x === to).length >= 4) {
            console.warn(`Node ${from.name} was unable to connect to ${to.name} because it is full.`);
            return false;
        }

        this.links[from.id] = to;
        this.edges.add(<Edge>{
            from: from.id,
            to: to.id,
            id: from.id
        });
        console.log(`[WIFI] Node ${from.name} is now connected to ${to.name}.`);
        return true;
    }

    public disconnect(me: Node): Node | undefined {
        let old = this.links[me.id];
        if (old) {
            this.links[me.id] = undefined;
            this.edges.remove(me.id);
            console.log(`[WIFI] Node ${me.name} is now disconnected. (was connected to ${old.name}).`);
            return old;
        }
    }

    private getSignalStrength(a: Node, b: Node) {
        // Calculate the distance in meters (minimal 10 cm).
        let distance = pythagoras(a.x - b.x, a.y - b.y);

        // Calculate the strength as 1/(.5d + 1), which yields 1 to 0 (exclusive) for 0 to infinity.
        let strength = 1 / (.2 * distance * distance + 1);

        // Calculate noise as 10^(x/4-0.25), which yields 0.526..1 for x = 0..1
        let noise = Math.pow(10, Math.random() / 4 - 0.25);

        return strength * noise;
    }
}

export let NodeKeeper = NodeKeeperImplementation.instance;
(<any>window)["NodeKeeper"] = NodeKeeper;