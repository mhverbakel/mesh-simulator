import { INodeAlgorithm, INodeActions, Direction, directions } from "../INodeAlgorithm";
import { IMessage, INodeInfo } from "../DTO";

export class AlgorithmA implements INodeAlgorithm {
    private readonly name: string;
    private readonly actions: INodeActions;
    private master?: string;
    private distance: number = 0;
    private nodeListPerDirection: { [direction: number]: INodeInfo[] }
        = new Array(5).fill(undefined).map(() => []);

    public constructor(name: string, actions: INodeActions) {
        this.name = name;
        this.actions = actions;

        this.think();
    }

    public messageReceived(direction: Direction, message: IMessage): void {
        switch (message.type) {
            case "welcome":
                /* We just joined a new network, and the master will be updated.
                   This message will always come from the parent. */
                if (direction !== Direction.Parent) throw 'Protocol violation';

                // Update the master and distance for existing nodes.
                this.setMaster(message.distance + 1, message.master);

                // Add new nodes to the node list.
                this.setDirectionNodes(direction, message.nodes);

                // Inform children about new master and new nodes.
                this.broadcastMessage(
                    {
                        type: "updateMaster",
                        master: message.master,
                        distance: message.distance + 1,
                        newNodes: message.nodes
                    },
                    direction);

                // Inform parent about new nodes.
                this.actions.sendMessage(
                    direction,
                    {
                        type: "updateNodes",
                        newNodes: this.nodeList(direction, true)
                    }
                )
                break;
            case "updateMaster":
                /* The upstream master got updated, and we must adjust accordingly.
                   This message will always come from the parent. */
                if (direction !== Direction.Parent) throw 'Protocol violation';
                
                // Update the master and distance for existing nodes.
                this.setMaster(message.distance + 1, message.master);

                // Add the new nodes.
                if (message.newNodes) {
                    this.pushDirectionNodes(direction, message.newNodes);
                }

                // Remove the lost nodes.
                if (message.lostNodes) {
                    this.popNodes(message.lostNodes);
                }

                // Pass the update along to the children.
                message.distance++;
                this.broadcastMessage(message, direction);
                break;
            case "updateNodes":
                // Add the new nodes.
                if (message.newNodes) {
                    this.pushDirectionNodes(direction, message.newNodes);
                }

                // Remove the lost nodes.
                if (message.lostNodes) {
                    this.popNodes(message.lostNodes);
                }

                this.broadcastMessage(message, direction);
                break;
        }
    }

    public connectionStarted(direction: Direction): void {
        if (direction === Direction.Parent) {
            // New parent.
            // Wait for the welcome message.
        } else {
            // New child.

            // Send a welcome message to inform the new node about the new master.
            this.actions.sendMessage(
                direction,
                {
                    type: "welcome",
                    master: this.master || this.name,
                    distance: this.distance,
                    nodes: this.nodeList(direction, true)
                });
        }
    }

    public connectionEnded(direction: Direction): void {
        if (direction === Direction.Parent) {
            /* Lost parent, become master. */

            // Remove nodes in the parent direction.
            let nodes = this.setDirectionNodes(direction);

            // Become master and update node distances.
            this.setMaster(0);

            // Inform children about the new master and lost nodes.
            this.broadcastMessage(
                {
                    type: "updateMaster",
                    master: this.name,
                    distance: 0,
                    lostNodes: nodes.map(x => x.name)
                },
                direction);
        } else {
            /* Lost child. */
            
            // Remove nodes in the given direction.
            let nodes = this.setDirectionNodes(direction);

            // Inform family about lost nodes.
            this.broadcastMessage(
                {
                    type: "updateNodes",
                    lostNodes: nodes.map(x => x.name)
                },
                direction);
        }
    }

    private async think() {
        
    }

    private broadcastMessage(message: IMessage, except?: Direction) {
        // Iterate over all directions.
        for(let direction of directions) {
            // If the direction is not excepted...
            if (direction !== except) {
                // ... send the message.
                this.actions.sendMessage(direction, message);
            }
        }
    }

    /**
     * Get the node list, possibly excluding a specific direction.
     * @param id If set, the direction that should be excluded.
     * @param self If set to true, includes this node in the list.
     */
    private nodeList(id?: number, self: boolean = false): INodeInfo[] {
        if (self)
            return [<INodeInfo>{
                distance: this.distance,
                name: this.name
            }].concat(this.nodeList(id, false));
        else
            return (<INodeInfo[][]>this.nodeListPerDirection)
                .filter((x, i) => i !== id)
                .reduce((a, b) => a.concat(b));
    }

    /**
     * Set the master to the current node, and update the node distances accordingly.
     * @param distance The distance between the current node and the master.
     */
    private setMaster(distance: 0): void;

    /**
     * Set the master to the specified node, and update the node distances accordingly.
     * @param distance The distance between the current node and the master.
     * @param name The name of the new master.
     */
    private setMaster(distance: number, name: string): void;

    private setMaster(distance: number, name?: string): void {
        let oldDistance = this.distance;
        this.master = name;
        this.distance = distance;
        
        let diff = distance - oldDistance;
        for (let node of this.nodeList()) {
            node.distance += diff;
        }
    }

    /**
     * Replaces the nodes in a given direction with the specified array.
     * @param direction The direction to set.
     * @param nodes The node list to set (or empty if undefined).
     * @returns The previous list of nodes.
     */
    private setDirectionNodes(direction: Direction, nodes: INodeInfo[] = []) {
        let old = this.nodeListPerDirection[<number>direction];
        this.nodeListPerDirection[<number>direction] = nodes;
        return old;
    }

    /**
     * Add the nodes to the given direction.
     * @param direction The direction to update.
     * @param nodes The nodes to add.
     */
    private pushDirectionNodes(direction: Direction, nodes: INodeInfo[]) {
        Array.prototype.push.apply(this.nodeListPerDirection[<number>direction], nodes);
    }

    /**
     * Remove the specified nodes from all directions.
     * @param names The names of the nodes to remove.
     */
    private popNodes(names: string[]) {
        // Check all directions.
        for (let direction of directions) {
            // Filter out nodes with the given names.
            this.nodeListPerDirection[<number>direction] =
                this.nodeListPerDirection[<number>direction].filter(
                    node => names.find(name => node.name === name) === undefined);
        }
    }
}