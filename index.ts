declare var __webpack_require__: any;
(<any>window).require = __webpack_require__;
import { NodeKeeper } from "./src/NodeKeeper";
import { DataSet, Network, Node as VisNode, Edge, Options } from "vis";

let nodes = NodeKeeper.visNodes;
let edges = NodeKeeper.edges;

let geoElement = document.getElementById("geoNet");
if (geoElement == null)
    throw "No container found!";

let geo = new Network(
    geoElement,
    {
        nodes,
        edges
    },
    <Options>{
        interaction: {
            dragNodes: false
        },
        physics: {
            enabled: false
        },
        edges: {
            arrows: "to"
        }
    });

let topoElement = document.getElementById("topoNet");
if (topoElement == null)
    throw "No container found!";

let topo = new Network(
    topoElement,
    {
        nodes,
        edges
    },
    <Options>{
        edges: {
            arrows: "to"
        }
    });

geo.on("selectNode", e => {
    topo.selectNodes(e.nodes);
});
topo.on("selectNode", e => {
    geo.selectNodes(e.nodes);
});

geo.on("deselectNode", e => {
    topo.selectNodes([]);
});
topo.on("deselectNode", e => {
    geo.selectNodes([]);
});